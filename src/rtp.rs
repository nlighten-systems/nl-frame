
/// Reliable Transport Protocol (RTP)
/// This simple protocol requires the receiver to 
/// send an acknowledgement packet
/// 
/// +-----+---------+---------+
/// | Ack | MsgType | Seq Num |
/// +-----+---------+---------+
/// 
/// 

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum MsgType {
    UnconfirmedDataUp =     0b0100_0000,
    UnconfirmedDataDown =   0b0110_0000,
    ConfirmedDataUp =       0b1000_0000,
    ConfirmedDataDown =     0b1010_0000,
    AckDataUp =             0b1100_0000,
    AckDataDown =           0b1110_0000,
}

impl TryFrom<u8> for MsgType {
    type Error = Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        let value = 0b1110_0000 & value;
        match value {
            x if x == MsgType::UnconfirmedDataUp as u8 => Ok(MsgType::UnconfirmedDataUp),
            x if x == MsgType::UnconfirmedDataDown as u8 => Ok(MsgType::UnconfirmedDataDown),
            x if x == MsgType::ConfirmedDataUp as u8 => Ok(MsgType::ConfirmedDataUp),
            x if x == MsgType::ConfirmedDataDown as u8 => Ok(MsgType::ConfirmedDataDown),
            x if x == MsgType::AckDataUp as u8 => Ok(MsgType::AckDataUp),
            x if x == MsgType::AckDataDown as u8 => Ok(MsgType::AckDataDown),
            _ => Err(Error::UnknownMsgType(value)),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct SeqNum(u8);

impl SeqNum {
    pub const MAX: u8 = 0b0001_1111;

    pub fn start() -> Self {
        SeqNum(3)
    }

    pub fn increment(&self) -> Self {
        let v = self.0 + 1;
        SeqNum(Self::MAX & v)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Header(MsgType, SeqNum);

impl Header {
    pub fn msgtype(&self) -> MsgType {
        self.0
    }

    pub fn seqnum(&self) -> SeqNum {
        self.1
    }

    pub fn ack(self) -> Result<Header, Error> {
        match self.0 {
            MsgType::ConfirmedDataUp => Ok(Header(MsgType::AckDataUp, self.1)),
            MsgType::ConfirmedDataDown => Ok(Header(MsgType::AckDataDown, self.1)),
            _ => Err(Error::InvalidType(self.0)),
        }
    }
}

impl TryFrom<u8> for Header {
    type Error = Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        let msg_type = MsgType::try_from(value)?;
        let seq_num = SeqNum(0b0001_1111 & value);
        Ok(Header(msg_type, seq_num))
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    UnknownMsgType(u8),
    InvalidType(MsgType),
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_seqnum_increment() {
        let n = SeqNum(3);
        let m = n.increment();
        assert_eq!(m.0, 4);
    }

    #[test]
    fn test_seqnum_increment_roll() {
        let n = SeqNum(SeqNum::MAX);
        let m = n.increment();
        assert_eq!(m.0, 0);
    }

    #[test]
    fn test_request_ack() {
        let h = Header(MsgType::ConfirmedDataUp, SeqNum(8));
        let h_ack = h.ack();
        assert_eq!(Ok(Header(MsgType::AckDataUp, SeqNum(8))), h_ack);
    }

    #[test]
    fn test_request_ack_unconfirmed() {
        let h = Header(MsgType::UnconfirmedDataUp, SeqNum(10));
        let h_ack = h.ack();

        // it is invalid to acknowledge a request that is not expected confirmation
        assert_eq!(Err(Error::InvalidType(MsgType::UnconfirmedDataUp)), h_ack);
    }
}

