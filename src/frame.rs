use core::ops::{Deref, DerefMut};

#[derive(Debug, Clone, Copy)]
pub struct Frame<const N: usize>([u8; N]);

impl<const N: usize> Frame<N> {
    pub const FRAME_SIZE: usize = N;
    pub const PAYLOAD_SIZE: usize = N - 1;
    pub const CHECKSUM_INIT: u8 = 0x34;

    pub const fn new() -> Self {
        Self([0_u8; N])
    }

    pub fn payload(&self) -> &[u8] {
        &self.0[0..(0 + Self::PAYLOAD_SIZE)]
    }

    pub fn payload_mut(&mut self) -> &mut [u8] {
        &mut self.0[0..(0 + Self::PAYLOAD_SIZE)]
    }

    pub fn checksum(&self) -> u8 {
        self.0[0 + Self::PAYLOAD_SIZE]
    }

    pub fn set_checksum(&mut self, checksum: u8) {
        self.0[0 + Self::PAYLOAD_SIZE] = checksum;
    }

    pub fn compute_checksum(&self) -> u8 {
        let mut checksum = Self::CHECKSUM_INIT;
        for b in self.payload() {
            checksum = checksum.wrapping_add(*b);
        }
        checksum
    }

    pub fn verify_checksum(&self) -> bool {
        self.checksum() == self.compute_checksum()
    }
}

impl<const N: usize> Deref for Frame<N> {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<const N: usize> DerefMut for Frame<N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}